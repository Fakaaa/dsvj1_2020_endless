#include "raylib.h"

#include "gameLoop.h"

#include "Gameplay/Screen/screen.h"
#include "Gameplay/gameplayHandle.h"
#include "Menu/menuHandeling.h"
#include "Sfx/Music/musicGame.h"

namespace Endless {
	namespace GameLoop {
		
		bool inGame;

		//----------------------------------------------------
		static void initAll() {
			inGame = true;
			//--------
			InitAudioDevice();
			//--------
			ScreenHandle::init();
			//--------
			MenuHanlde::init();
			//--------
			MusicHandle::init();
			//--------
			GameManager::initGameElements();
			//--------
		}
		//----------------------------------------------------
		static void deinitAll() {
			//--------
			GameManager::deinitGameElements();
			//--------
			MenuHanlde::deinit();
			//--------
			MusicHandle::deinit();
			//--------
		}
		//----------------------------------------------------
		static void drawAll() {
			//-------------
			ClearBackground(BLACK);
			BeginDrawing();
			//-------------
			if (MenuHanlde::onMenu) {
				//------------
				MenuHanlde::draw();
				//------------
			}
			else{
				//------------
				GameManager::drawGameElements();
				//------------
			}
			//------------
			EndDrawing();
			//------------
		}
		//----------------------------------------------------
		static void update() {
			if (MenuHanlde::onMenu) {
				//------------
				MenuHanlde::update();
				//------------
				MusicHandle::update();
				//------------
			}
			else if (!MenuHanlde::onMenu) {
				//------------
				GameManager::inputsGame();
				//------------
				MusicHandle::update();
				//------------
				GameManager::updateGameElements();
				//------------
			}
		}
		//----------------------------------------------------
		void game(){
			//------------
			initAll();
			//------------
			while (inGame && !WindowShouldClose())
			{
				//------------
				update();
				//------------
				drawAll();
				//------------
			}
			//------------
			deinitAll();
			//------------
		}
		//----------------------------------------------------
	}
}