#include <iostream>

#include "player.h"

#include "Gameplay/Screen/screen.h"
#include "Gameplay/Scene/scene.h"
#include "Gameplay/Obstacles/obstacles.h"
#include "Sfx/Sounds/soundsGame.h"

using namespace Endless;
using namespace ScreenHandle;
using namespace ObstaclesManager;

namespace Endless {
	namespace Player {
	//----------------------------------------------------------------------
	//PJ VARIABLES
	//----------------------------------------------------------------------
	const int FRAMES_DIV_RUN = 7;
	const int FRAMES_DIV_JUMP = 8;
	const int FRAMES_DIV_CRASH = 5;
	//----------------------------------------------------------------------
	SOMEGUY guy;
	static float POSx = static_cast<float>(actualWidth / 5);
	float WIDTH = 90.0f;
	float HEIGTH = 100.0f;
	static float FRAME_WIDTH = 470.0f / FRAMES_DIV_RUN;
	static float FRAME_HEIGTH = 55.0f;
	//----------------------------------------------------------------------
	static int currentFrame = 0;
	static int frameCounter = 0;
	static int frameSpeedRun = 8;
	static int frameSpeedJump = 7;
	static int frameSpeedCrash = 4;
	static int frameSpeedEvade = 8;
	//----------------------------------------------------------------------
	static float scorePosX = 0.0f;
	static float scorePosY = 0.0f;
	static bool firstInit = false;
	bool colectablePicked = false;
	//----------------------------------------------------------------------
	static float timePassed = 0.0f;
	static float timeToWaitGover = 1.0f;
	//----------------------------------------------------------------------
	static int fontSScore;
	static float scalarSpeed;
	//----------------------------------------------------------------------
	ANIMATIONS anim;
	//----------------------------------------------------------------------
	Texture2D scoreplayer;
	//----------------------------------------------------------------------
	//PJ CORE METHODS
	//----------------------------------------------------------------------
	void init() {
		POSx = static_cast<float>(actualWidth / 5);
		WIDTH = static_cast<float>(actualWidth / 16.0f);
		HEIGTH = static_cast<float>(actualHeigth / 9.0f);
		FRAME_WIDTH = 470.0f / FRAMES_DIV_RUN;
		FRAME_HEIGTH = 55.0f;
		//---------------------------------------CORE PLAYER
		guy.collider = { POSx ,static_cast<float>(actualHeigth / toPoseLayer0) - 100, WIDTH, HEIGTH };
		switch (whatReso)
		{
		case Endless::ScreenHandle::_low:
			scalarSpeed = 1.40f;
			fontSScore = 22;
			guy.speed = 333.0f;
			guy.gravInfluence = 261.5f;			//REGLA DE TRES SIMPLE
			break;
		case Endless::ScreenHandle::_medium:
			scalarSpeed = 1.85f;
			fontSScore = 30;
			guy.speed = 450.0f;
			guy.gravInfluence = 352.5f;
			break;
		case Endless::ScreenHandle::_high:
			scalarSpeed = 2.5f;
			fontSScore = 40;
			guy.speed = 600.0f;
			guy.gravInfluence = 470.0f;
			break;
		}
		//---------------------------------------BOOLS PLAYER
		guy.alive = true;
		guy.jumping = false;
		guy.falling = false;
		guy.onGround = NULL;
		guy.onChangeLayerDown = false;
		guy.onChangeLayerUp = false;
		guy.evading = false;
		//---------------------------------------
		timePassed = 0.0f;
		timeToWaitGover = 1.0f;
		guy.score = 0;
		scorePosX = static_cast<float>((actualWidth / 2) - 100);
		scorePosY = 10;
		guy.layer = 0;
		if (!firstInit) {
			loadTexture();
			firstInit = true;
		}
		FRAME_WIDTH = static_cast<float>(guy.player[RUNNING-1].width / FRAMES_DIV_RUN);
		FRAME_HEIGTH = static_cast<float>(guy.player[RUNNING-1].height);
		guy.TEXTURE_POS = {guy.collider.x , guy.collider.y + 10 };
		guy.frameRec = { 0.0f,0.0f, FRAME_WIDTH , FRAME_HEIGTH };
		guy.ANIM_STATE = 5;
	}
	//-----------------
	void inputs() {

		if (IsKeyPressed(KEY_SPACE) && guy.onGround) {
			SoundsManager::stopFootsteps();
			SoundsManager::stopJump();
			SoundsManager::playJump();
			guy.evading = false;
			guy.ANIM_STATE = 2;
			guy.jumping = true;
			currentFrame = 8;
			guy.collider.height = HEIGTH / 2;
		}
		if (IsKeyPressed(KEY_DOWN) && guy.onGround) {
			guy.evading = false;
			guy.onChangeLayerDown = true;
		}
		else if (IsKeyPressed(KEY_UP) && guy.onGround) {
			guy.evading = false;
			guy.onChangeLayerUp = true;
		}
		if (IsKeyPressed(KEY_V)) {
			SoundsManager::stopFootsteps();
			currentFrame = -1;
			guy.ANIM_STATE = 6;
			guy.evading = true;
			guy.collider.width = WIDTH / 2;
		}
		if (IsKeyPressed(KEY_F)) {
			guy.evading = false;
			guy.alive = true;
			guy.collider.y = static_cast<float>(0.0f);
			guy.ANIM_STATE = 5;
		}

		if (IsKeyPressed(KEY_ESCAPE)) {
			SoundsManager::stopJump();
			SoundsManager::stopFootsteps();
			GameManager::onPause = true;
		}
	}
	//-----------------
	void draw() {
#if DEBUG
		DrawRectangleLinesEx(guy.collider, 5, GREEN);
		DrawRectangleLinesEx(guy.frameRec, 5, WHITE);
#endif	
		switch (guy.ANIM_STATE)
		{
		case Endless::Player::RUNNING:
			DrawTextureRec(guy.player[0], guy.frameRec, guy.TEXTURE_POS, WHITE);
			break;
		case Endless::Player::JUMPING:
			DrawTextureRec(guy.player[1], guy.frameRec, guy.TEXTURE_POS, WHITE);
			break;
		case Endless::Player::CRASHING:
			DrawTextureRec(guy.player[2], guy.frameRec, guy.TEXTURE_POS, WHITE);
			break;
		case Endless::Player::DEAD:
			DrawTextureRec(guy.player[3], guy.frameRec, guy.TEXTURE_POS, WHITE);
			break;
		case Endless::Player::FALLING:
			DrawTextureRec(guy.player[4], guy.frameRec, guy.TEXTURE_POS, WHITE);
			break;
		case Endless::Player::EVADING:
			DrawTextureRec(guy.player[5], guy.frameRec, guy.TEXTURE_POS, WHITE);
			break;
		default:
			std::cout << "ERROR08: Fallo el switch de draw player!" << std::endl;
			break;
		}

		DrawTexture(scoreplayer, static_cast<int>(scorePosX - scoreplayer.width / 2), static_cast<int>(scorePosY-30), WHITE);
		DrawText(FormatText("%i", guy.score), static_cast<int>(scorePosX + (scoreplayer.width / 2)), static_cast<int>(scorePosY), fontSScore, WHITE);
		if(colectablePicked)
			DrawText(FormatText("+%i", 500), static_cast<int>(scorePosX + (scoreplayer.width / 2)), static_cast<int>(scorePosY + 40), fontSScore, WHITE);
	}
	//-----------------
	static void calcSpriteRec(int DIVS, int frameSpeed, int whatAnim) {
		frameCounter++;

		if (frameCounter >= ( ScreenHandle::maxFPS / frameSpeed)) {
			frameCounter = 0;
			currentFrame++;

			if (currentFrame >= DIVS) {
				currentFrame = 0;
				if(whatAnim == 2)
					guy.ANIM_STATE = 4;
			} 

			guy.frameRec.x = (float)currentFrame*(float)(guy.player[whatAnim].width / DIVS);
		}
		if (frameSpeed >= MAX_FRAME_SPEED) frameSpeed = MAX_FRAME_SPEED;
		else if (frameSpeed <= MIN_FRAME_SPEED) frameSpeed = MIN_FRAME_SPEED;
	}
	//-----------------
	void deinit() {
		unloadTexture();
	}
	//----------------------------------------------------------------------
	void loadTexture(){
		//--------------------------
		Image image;
		if (!ScreenHandle::onRezise) {
			//--------------------------
			image = LoadImage("res/assets/player/Running.png");
			ImageResize(&image, static_cast<int>(WIDTH*FRAMES_DIV_RUN), static_cast<int>(HEIGTH));
			guy.player[0] = LoadTextureFromImage(image);
			UnloadImage(image);
			//--------------------------
			image = LoadImage("res/assets/player/Jumping.png");
			ImageResize(&image, static_cast<int>((WIDTH+20)*FRAMES_DIV_JUMP), static_cast<int>(HEIGTH));
			guy.player[1] = LoadTextureFromImage(image);
			UnloadImage(image);
			//--------------------------
			image = LoadImage("res/assets/player/Crash.png");
			ImageResize(&image, static_cast<int>((WIDTH)*FRAMES_DIV_CRASH), static_cast<int>(HEIGTH));
			guy.player[2] = LoadTextureFromImage(image);
			UnloadImage(image);
			//--------------------------
			image = LoadImage("res/assets/player/Death.png");
			ImageResize(&image, static_cast<int>((WIDTH)*FRAMES_DIV_CRASH), static_cast<int>(HEIGTH));
			guy.player[3] = LoadTextureFromImage(image);
			UnloadImage(image);
			//--------------------------
			image = LoadImage("res/assets/player/Falling.png");
			ImageResize(&image, static_cast<int>((WIDTH)*FRAMES_DIV_CRASH), static_cast<int>(HEIGTH));
			guy.player[4] = LoadTextureFromImage(image);
			UnloadImage(image);
			//--------------------------
			image = LoadImage("res/assets/player/Evade.png");
			ImageResize(&image, static_cast<int>((WIDTH)*FRAMES_DIV_CRASH), static_cast<int>(HEIGTH));
			guy.player[5] = LoadTextureFromImage(image);
			UnloadImage(image);
			//--------------------------
			image = LoadImage("res/assets/player/Score.png");
			ImageResize(&image, static_cast<int>(actualWidth / 6), static_cast<int>(actualHeigth / 8));
			scoreplayer = LoadTextureFromImage(image);
			UnloadImage(image);
			//--------------------------
		}
		else {
			unloadTexture();
			//--------------------------
			image = LoadImage("res/assets/player/Running.png");
			ImageResize(&image, static_cast<int>(WIDTH*FRAMES_DIV_RUN), static_cast<int>(HEIGTH));
			guy.player[0] = LoadTextureFromImage(image);
			UnloadImage(image);
			//--------------------------
			image = LoadImage("res/assets/player/Jumping.png");
			ImageResize(&image, static_cast<int>((WIDTH + 20)*FRAMES_DIV_JUMP), static_cast<int>(HEIGTH));
			guy.player[1] = LoadTextureFromImage(image);
			UnloadImage(image);
			//--------------------------
			image = LoadImage("res/assets/player/Crash.png");
			ImageResize(&image, static_cast<int>((WIDTH)*FRAMES_DIV_CRASH), static_cast<int>(HEIGTH));
			guy.player[2] = LoadTextureFromImage(image);
			UnloadImage(image);
			//--------------------------
			image = LoadImage("res/assets/player/Death.png");
			ImageResize(&image, static_cast<int>((WIDTH)*FRAMES_DIV_CRASH), static_cast<int>(HEIGTH));
			guy.player[3] = LoadTextureFromImage(image);
			UnloadImage(image);
			//--------------------------
			image = LoadImage("res/assets/player/Falling.png");
			ImageResize(&image, static_cast<int>((WIDTH)*FRAMES_DIV_CRASH), static_cast<int>(HEIGTH));
			guy.player[4] = LoadTextureFromImage(image);
			UnloadImage(image);
			//--------------------------
			image = LoadImage("res/assets/player/Evade.png");
			ImageResize(&image, static_cast<int>((WIDTH)*FRAMES_DIV_CRASH), static_cast<int>(HEIGTH));
			guy.player[5] = LoadTextureFromImage(image);
			UnloadImage(image);
			//--------------------------
			image = LoadImage("res/assets/player/Score.png");
			ImageResize(&image, static_cast<int>(actualWidth / 6), static_cast<int>(actualHeigth / 8));
			scoreplayer = LoadTextureFromImage(image);
			UnloadImage(image);
			//--------------------------
		}
	}
	//----------------------------------------------------------------------
	void unloadTexture(){
		for (int i = 0; i < STATE_ANIM; i++){
			UnloadTexture(guy.player[i]);
		}
	}
	//----------------------------------------------------------------------
	//UPDATE / CHANGE PJ POS AND ETC
	//----------------------------------------------------------------------
	void update() {
		if (guy.ANIM_STATE == RUNNING) {
			SoundsManager::playFootsteps();
			FRAME_WIDTH = static_cast<float>(guy.player[0].width / FRAMES_DIV_RUN);
			FRAME_HEIGTH = static_cast<float>(guy.player[0].height);
			if(guy.alive)
				calcSpriteRec(FRAMES_DIV_RUN,frameSpeedRun,0);
		}
		else if(guy.ANIM_STATE == JUMPING){
			FRAME_WIDTH = static_cast<float>(guy.player[1].width / FRAMES_DIV_JUMP);
			FRAME_HEIGTH = static_cast<float>(guy.player[1].height);
			if (guy.alive)
				calcSpriteRec(FRAMES_DIV_JUMP,frameSpeedJump,1);
		}
		else if (guy.ANIM_STATE == CRASHING) {
			FRAME_WIDTH = static_cast<float>(guy.player[2].width / FRAMES_DIV_CRASH);
			FRAME_HEIGTH = static_cast<float>(guy.player[2].height);
			calcSpriteRec(FRAMES_DIV_CRASH, frameSpeedCrash, 2);
		}
		else if (guy.ANIM_STATE == DEAD) {
			FRAME_WIDTH = static_cast<float>(guy.player[3].width);
			FRAME_HEIGTH = static_cast<float>(guy.player[3].height);
			calcSpriteRec(FRAMES_DIV_CRASH, frameSpeedCrash, 3);
		}
		else if (guy.ANIM_STATE == FALLING) {
			FRAME_WIDTH = static_cast<float>(guy.player[4].width / FRAMES_DIV_CRASH);
			FRAME_HEIGTH = static_cast<float>(guy.player[4].height);
			calcSpriteRec(FRAMES_DIV_CRASH, frameSpeedCrash, 4);
		}
		
		if (guy.ANIM_STATE == EVADING) {
			FRAME_WIDTH = static_cast<float>(guy.player[5].width / FRAMES_DIV_CRASH);
			FRAME_HEIGTH = static_cast<float>(guy.player[5].height);
			if (guy.alive)
				calcSpriteRec(FRAMES_DIV_CRASH, frameSpeedEvade, 5);
		}
		if (guy.alive)
			applyPhysics();

		scorePosX = static_cast<float>((ScreenHandle::actualWidth / 2) - 100);

		if (!guy.alive)
			timePassed += GetFrameTime();
		timeToWaitGover = 1.0f;

		std::cout << timePassed << std::endl;
		std::cout << timeToWaitGover << std::endl;

		if (!guy.alive && (timePassed >= timeToWaitGover)) {
			GameManager::onGameOver = true;
			timePassed = 0;
		}
		if (guy.collider.y > actualHeigth + guy.collider.height) {
			guy.alive = false;
			guy.falling = false;
		}
	}
	//----------------------------------------------------------------------
	//PHYSICS
	//----------------------------------------------------------------------
	void applyPhysics() {

		if (guy.jumping) {
			guy.collider.y -= (guy.speed * GetFrameTime())*1.5f;
		}
		if (guy.onChangeLayerDown) {
			guy.layer++;
		}
		else if (guy.onChangeLayerUp) {
			guy.layer--;
		}
		if (guy.layer > 2)
			guy.layer = 2;
		else if (guy.layer < 0)
			guy.layer = 0;

#if DEBUG
		//std::cout << "[0 false | 1 true] Guy alive: " << guy.ailve << std::endl;
		std::cout << "Current frame: " << currentFrame << std::endl;
#endif
		makeTransAnimations();

		switch (guy.layer)
		{
		case 0:
			if (CheckCollisionRecs(guy.collider, SceneManagerGame::scene.floorLayer0) && !guy.falling) {
				guy.onGround = true;
				guy.collider.y = guy.collider.y;
				guy.collider.x = guy.collider.x;
				switch (whatReso)
				{
				case Endless::ScreenHandle::_low:
					guy.speed = 333.0f;
					break;
				case Endless::ScreenHandle::_medium:
					guy.speed = 450.0f;
					break;
				case Endless::ScreenHandle::_high:
					guy.speed = 600.0f;
					break;
				}
				guy.jumping = false;
				if(!guy.evading)
					guy.ANIM_STATE = 1;
			}
			else {
				guy.onGround = false;
				guy.collider.y = guy.collider.y + (guy.gravInfluence * GetFrameTime());
				guy.speed = guy.speed - (guy.gravInfluence * GetFrameTime());
			}
			if (guy.onChangeLayerUp || guy.onChangeLayerDown) {
				guy.collider.y = static_cast<float>(toPoseLayer0- HEIGTH);
				guy.collider.x = static_cast<float>(actualWidth / 5);
				guy.onChangeLayerUp = false;
				guy.onChangeLayerDown = false;
			}
			break;
		case 1:
			if (CheckCollisionRecs(guy.collider, SceneManagerGame::scene.floorLayer1) && !guy.falling) {
				guy.onGround = true;
				guy.collider.y = guy.collider.y;
				guy.collider.x = guy.collider.x;
				switch (whatReso)
				{
				case Endless::ScreenHandle::_low:
					guy.speed = 333.0f;
					break;
				case Endless::ScreenHandle::_medium:
					guy.speed = 450.0f;
					break;
				case Endless::ScreenHandle::_high:
					guy.speed = 600.0f;
					break;
				}
				guy.jumping = false;
				if (!guy.evading)
					guy.ANIM_STATE = 1;
			}
			else {
				guy.onGround = false;
				guy.collider.y = guy.collider.y + (guy.gravInfluence * GetFrameTime());
				guy.speed = guy.speed - (guy.gravInfluence * GetFrameTime());
			}
			if (guy.onChangeLayerUp || guy.onChangeLayerDown) {
				guy.collider.y = static_cast<float>(toPoseLayer1 - HEIGTH);
				guy.collider.x = static_cast<float>(actualWidth / 5);
				guy.onChangeLayerUp = false;
				guy.onChangeLayerDown = false;
			}
			break;
		case 2:
			if (CheckCollisionRecs(guy.collider, SceneManagerGame::scene.floorLayer2) && !guy.falling) {
				guy.onGround = true;
				guy.collider.y = guy.collider.y;
				guy.collider.x = guy.collider.x;
				switch (whatReso)
				{
				case Endless::ScreenHandle::_low:
					guy.speed = 333.0f;
					break;
				case Endless::ScreenHandle::_medium:
					guy.speed = 450.0f;
					break;
				case Endless::ScreenHandle::_high:
					guy.speed = 600.0f;
					break;
				}
				guy.jumping = false;
				if (!guy.evading)
					guy.ANIM_STATE = 1;
			}
			else {
				guy.onGround = false;
				guy.collider.y = guy.collider.y + (guy.gravInfluence * GetFrameTime());
				guy.speed = guy.speed - (guy.gravInfluence * GetFrameTime());
			}
			if (guy.onChangeLayerUp || guy.onChangeLayerDown) {
				guy.collider.y = static_cast<float>(toPoseLayer2 - HEIGTH);
				guy.collider.x = static_cast<float>(actualWidth / 5);
				guy.onChangeLayerUp = false;
				guy.onChangeLayerDown = false;
			}
			break;
		}

		if ((CheckCollisionRecs(guy.collider, obstacle2.badThing)
				&& obstacle2.layer == guy.layer)) {
			if (obstacle2.typeObs == pipe) {
				guy.ANIM_STATE = 3;
				guy.alive = false;
			}
			if (obstacle2.typeObs == waterTank && !guy.evading) {
				guy.ANIM_STATE = 3;
				guy.alive = false;
			}
			if (obstacle2.typeObs == rooftopDoor) {
				guy.ANIM_STATE = 3;
				guy.alive = false;
			}
		}
		else if(guy.alive){
			guy.TEXTURE_POS = { guy.collider.x , guy.collider.y + 10};
			guy.score++;
			ObstaclesManager::speedObstacles += scalarSpeed * GetFrameTime();
			frameSpeedRun += static_cast<int>(1.5f*GetFrameTime());
			if(guy.gravInfluence <= 505.0f)
				guy.gravInfluence += static_cast<float>(0.5f*GetFrameTime());
		}
		if ((CheckCollisionRecs(guy.collider, obstacle3.badThing)
			&& obstacle3.layer == guy.layer)) {
			if (obstacle3.typeObs == pipe) {
				guy.ANIM_STATE = 3;
				guy.alive = false;
			}
			if (obstacle3.typeObs == waterTank && !guy.evading) {
				guy.ANIM_STATE = 3;
				guy.alive = false;
			}
			if (obstacle3.typeObs == rooftopDoor) {
				guy.ANIM_STATE = 3;
				guy.alive = false;
			}
		}
		if (shuriken.isPlaced && (CheckCollisionRecs(guy.collider, shuriken.badThing)
			&& shuriken.layer == guy.layer)) {
			SoundsManager::stopGetShuri();
			SoundsManager::playGetShuri();
			guy.score += 500;
			colectablePicked = true;
			shuriken.isPlaced = false;
		}

		if (guy.onGround && CheckCollisionRecs(guy.collider, obstacle1.badThing) && obstacle1.typeObs == roofdrop){ 
			if (guy.collider.x >= obstacle1.badThing.x) {
				guy.ANIM_STATE = 5;
				guy.falling = true; 
			}
		}
	}
	//----------------------------------------------------------------------
	void makeTransAnimations(){

		if (guy.ANIM_STATE == 2 && currentFrame == 5) {
			guy.collider.height = HEIGTH;
			FRAME_WIDTH = static_cast<float>(guy.player[0].width / FRAMES_DIV_RUN);
			FRAME_HEIGTH = static_cast<float>(guy.player[0].height);
			if (guy.alive)
				calcSpriteRec(FRAMES_DIV_RUN, frameSpeedRun, 0);
		}
		if (guy.ANIM_STATE == 6 && currentFrame == 4) {
			guy.collider.width = WIDTH;
			guy.evading = false;
		}

		if (guy.ANIM_STATE == 2 && currentFrame == FRAMES_DIV_JUMP) {
			currentFrame = 8;
		}
		if (guy.ANIM_STATE == 6 && currentFrame == FRAMES_DIV_CRASH) {
			currentFrame = 0;
		}
	}
	//----------------------------------------------------------------------
	//OTHERS
	//----------------------------------------------------------------------
	void resizeOnResolution(){
		//12 NUMERO HERMOSO PARA EL WIDTH | 5,5 numero 
		guy.collider.y = static_cast<float>(0.0f);
		guy.collider.x = static_cast<float>(actualWidth / 5);
		WIDTH = (float)(actualWidth / 16.0f);
		HEIGTH = (float)(actualHeigth / 9.0f);
		loadTexture();

		FRAME_WIDTH = static_cast<float>(guy.player[RUNNING - 1].width / FRAMES_DIV_RUN);
		FRAME_HEIGTH = static_cast<float>(guy.player[RUNNING - 1].height);
		guy.TEXTURE_POS = { guy.collider.x , guy.collider.y + 10 };
		guy.frameRec = { 0.0f,0.0f, FRAME_WIDTH , FRAME_HEIGTH };
	}
	//----------------------------------------------------------------------
	}
}