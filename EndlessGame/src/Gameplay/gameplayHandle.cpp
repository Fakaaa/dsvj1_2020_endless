#include <iostream>

#include "gameplayHandle.h"

#include "Gameplay/Scene/scene.h"
#include "Gameplay/Obstacles/obstacles.h"
#include "Gameplay/Player/player.h"
#include "Sfx/Music/musicGame.h"
#include "Menu/Buttons/buttons.h"
#include "Menu/menuHandeling.h"

using namespace Endless;

namespace Endless {
	namespace GameManager {
		//-------------------------------
		static int pauseKey = 0;
		static int goverKey = 0;
		//-------------------------------
		bool onPause;
		bool onGameOver;
		bool selectThat;
		//-------------------------------
		bool onGameplay;
		//-------------------------------
		GAME_STATE gameState;
		//-------------------------------
		//PLAY_STATE playerOnGame;
		//-------------------------------
		void initGameElements(){
			gameState = GOMENU;
			//-------------
			onGameplay = true;
			//-------------
			SceneManagerGame::init();
			//-------------
			Player::init();
			//-------------
			ObstaclesManager::initObstacle();
			//-------------
		}
		//-------------------------------
		void inputsGame(){
			//-------------
			Player::inputs();
			//-------------
		}
		//-------------------------------
		void reinitOnDead(){
			//-------------
			Player::init();
			//-------------
			ObstaclesManager::reinit();
			//-------------
		}
		//-------------------------------
		void startGameSoundtrack(){
			//-------------
			if (onGameplay) 
				PlayMusicStream(MusicHandle::soudntrackGame.soundTrack);
			//-------------
		}
		//-------------------------------
		void stopGameSoundtrack(){
			//-------------
			if(!onGameplay)
				StopMusicStream(MusicHandle::soudntrackGame.soundTrack);
			//-------------
		}
		//-------------------------------
		void drawGameElements(){
			//-------------
			if (!onPause) {
				if (!onGameOver) {
					SceneManagerGame::draw();
				}
				else {
					SceneManagerGame::drawLastFrame();
					ButtonHandle::draw_restart();
				}
			}
			else {
				SceneManagerGame::drawLastFrame();
				ButtonHandle::draw_pause();
			}
			//-------------
		}
		//-------------------------------
		void handleKeys(int& keysToHandle){
			//-----------------------------
			if (IsKeyPressed(KEY_DOWN)) {
				if (keysToHandle <= GOMENU) {
					keysToHandle++;
				}
			}
			else if (IsKeyPressed(KEY_UP)) {
				if (keysToHandle >= RESUME) {
					keysToHandle--;
				}
			}
			if (keysToHandle < RESUME)keysToHandle = GOMENU;
			else if (keysToHandle > GOMENU)keysToHandle = RESUME;
			//-----------------------------
			if (IsKeyPressed(KEY_ENTER))
				selectThat = true;
			//-----------------------------
		}
		//-------------------------------
		static void resumeOption() {
			ButtonHandle::RESUME.state = true;
			ButtonHandle::GOMENU.state = false;
			if (selectThat) {
				onPause = false;
				selectThat = false;
				ResumeMusicStream(MusicHandle::soudntrackGame.soundTrack);
			}
		}
		//-------------------------------
		static void gomenuOption() {
			ButtonHandle::RESUME.state = false;
			ButtonHandle::GOMENU.state = true;
			if (selectThat) {
				onGameplay = false;
				stopGameSoundtrack();
				MenuHanlde::onMenu = true;
				PlayMusicStream(MusicHandle::soudntrackMenu.soundTrack);
				selectThat = false;
				reinitOnDead();
				onPause = false;
			}
		}
		//-------------------------------
		static void gameOverRestart() {
			ButtonHandle::RESTART.state = true;
			ButtonHandle::GOMENU.state = false;
			if (selectThat) {
				onGameOver = false;
				selectThat = false;
				reinitOnDead();
				PlayMusicStream(MusicHandle::soudntrackGame.soundTrack);
			}
		}
		static void gameOverTomenu() {
			ButtonHandle::RESTART.state = false;
			ButtonHandle::GOMENU.state = true;
			if (selectThat) {
				onGameplay = false;
				onGameOver = false;
				stopGameSoundtrack();
				MenuHanlde::onMenu = true;
				PlayMusicStream(MusicHandle::soudntrackMenu.soundTrack);
				selectThat = false;
				reinitOnDead();
				onPause = false;
			}
		}
		//-------------------------------
		void updateGameElements(){
			//-------------
			if (!onPause) {

				if (!onGameOver) {
					//-------------
					SceneManagerGame::update();
					//-------------
					Player::update();
					//-------------
					ObstaclesManager::update();
					//-------------
				}
				else {
					handleKeys(goverKey);

					StopMusicStream(MusicHandle::soudntrackGame.soundTrack);

					switch (goverKey)
					{
					case Endless::GameManager::RESUME:	gameOverRestart();
						break;
					case Endless::GameManager::GOMENU:	gameOverTomenu();
						break;
					default:
						std::cout << "ERROR06: Fallo switch gameplayHanlde draw!" << std::endl;
						break;
					}
				}
			}
			else {
				handleKeys(pauseKey);

				PauseMusicStream(MusicHandle::soudntrackGame.soundTrack);

				switch (pauseKey)
				{
				case Endless::GameManager::RESUME:	resumeOption();
					break;
				case Endless::GameManager::GOMENU:	gomenuOption();
					break;
				default:
					std::cout << "ERROR06: Fallo switch gameplayHanlde draw!" << std::endl;
					break;
				}
			}
			//-------------
		}
		//-------------------------------
		void deinitGameElements(){
			//-------------
			Player::deinit();
			//-------------
			SceneManagerGame::deinit();
			//-------------
			ObstaclesManager::unloadTextures();
			//-------------
		}
		//-------------------------------
	}
}