#ifndef GAMEPLAYHANDLE_H
#define GAMEPLAYHANDLE_H

namespace Endless {
	namespace GameManager {
		//-----------------
		enum GAME_STATE{
			RESUME,
			GOMENU
		};
		//-----------------
		//enum PLAY_STATE {
		//	RESTART,
		//	TOMENU
		//};
		//-----------------
		extern GAME_STATE gameState;
		//-----------------
		//extern PLAY_STATE playerOnGame;
		//-----------------
		extern bool onPause;
		extern bool selectThat;
		//-----------------
		extern bool onGameplay;
		extern bool onGameOver;
		//-----------------

		void initGameElements();
		void inputsGame();
		void reinitOnDead();
		void startGameSoundtrack();
		void stopGameSoundtrack();
		void drawGameElements();
		void handleKeys(int& keysToHandle);
		void updateGameElements();
		void deinitGameElements();
	}
}
#endif // !GAMEPLAYHANDLE_H