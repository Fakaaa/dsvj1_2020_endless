#ifndef SCREEN_H
#define SCREEN_H

#include "raylib.h"

namespace Endless {
	namespace ScreenHandle {

		struct RESOLUTIONS
		{
			int lowWidth = 800;
			int lowHeigth = 600;
			//---------
			int mediumWidth = 1080;
			int mediumHeigth = 720;
			//---------
			int highWidth = 1440;
			int highHeigth = 900;
		};
		//---------
		enum ONTHAT_RESOLU{
			_low,
			_medium,
			_high
		};
		//---------
		extern bool onRezise;
		//---------
		extern ONTHAT_RESOLU whatReso;
		//---------
		extern RESOLUTIONS chooseWindow;
		//---------
		extern float toPoseLayer0;
		extern float toPoseLayer1;
		extern float toPoseLayer2; 
		extern float posRoof;
		//---------
		extern int maxFPS;
		extern int actualWidth;
		extern int actualHeigth;
		//---------
		extern int defaultWidth;
		extern int defaultHeigth;
		//---------
		void init();
		void setWindowResolution(int choose);
		void changeResolution(int resToChange);
	}
}

#endif // !SCREEN_H