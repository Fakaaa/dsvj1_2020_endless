#ifndef OBSTACLES_H
#define OBSTACLES_H

#include "raylib.h"

namespace Endless {
	namespace ObstaclesManager {
		//------------------------------
		const int amountObstacles = 4;
		//------------------------------
		enum TYPE
		{
			roofdrop = 1,
			pipe,
			waterTank,
			rooftopDoor,
			colectable
		};
		//------------------------------
		extern TYPE typeObstacle;
		//------------------------------
		extern bool resiszeAfterResolu;
		//------------------------------
		struct OBSTACLES
		{
			Rectangle badThing;
			Texture2D badThingTexture;
			TYPE typeObs;
			bool isPlaced;
			int layer;
		};
		//------------------------------
		extern OBSTACLES obstacle1;
		extern OBSTACLES obstacle2;
		extern OBSTACLES obstacle3;
		extern OBSTACLES shuriken;
		//------------------------------
		extern float speedObstacles;

		void reinit();
		void initObstacle();
		void genObstacles();
		void createObstacle(OBSTACLES &obstacle, int randType, float randX, int randY);
		void update();
		void updateObstacles(OBSTACLES &obstacle);
		void draw();
		void drawRoofall();
		void reziseOnResolution();
		void loadTextures();
		void unloadTextures();
	}
}
#endif // !OBSTACLES_H