#include "obstacles.h"

#include "Gameplay/Screen/screen.h"
#include "Gameplay/Scene/scene.h"
#include "Gameplay/Player/player.h"

using namespace Endless;
using namespace ScreenHandle;
using namespace Player;

namespace Endless {
	namespace ObstaclesManager {
		//------------------------------
		OBSTACLES obstacle1;
		OBSTACLES obstacle2;
		OBSTACLES obstacle3;
		OBSTACLES shuriken;
		TYPE typeObstacle;
		//------------------------------
		bool resiszeAfterResolu = false;
		//------------------------------
		//-----------ESPECIFICO----------
		static int randomTypeObs1 = 0;
		static float randomXObs1 = 0;
		static int randomYObs1 = 0;
		//---------------------
		static int randomTypeObs2 = 0;
		static float randomXObs2 = 0;
		static int randomYObs2 = 0;
		//---------------------
		static int randomTypeObs3 = 0;
		static float randomXObs3 = 0;
		static int randomYObs3 = 0;
		//---------------------
		static float posXShuri = 0;
		static int posYShuri = 0;
		static float widthShuriken;
		static float heightShuriken;
		//-----------GENERICO--------------
		static float randPosY0 = 0;
		static float randPosY1 = 0;
		static float randPosY2 = 0;
		//-------------------------------
		static float heigthRoof = 0;
		static float heigthPipe = 0;
		static float heigthWaterTank = 0;
		static float heigthRooftopDoor = 0;
		static float ROOFFALLtextureY = 0.0f;

		//------------------------------
		float speedObstacles;
		//------------------------------
		Color colorType;
		Color colorType2;
		//------------------------------
		bool alreadyLoaded = false;
		//------------------------------
		Texture2D auxTextureObstacle2[amountObstacles];
		//------------------------------
		void reinit() {
			//-------------------------------
			obstacle1.isPlaced = false;
			obstacle2.isPlaced = false;
			obstacle3.isPlaced = false;
			shuriken.isPlaced = false;
			//-------------------------------
			obstacle1.badThing = { 0.0f,0.0f,0.0f,0.0f };
			obstacle2.badThing = { 0.0f,0.0f,0.0f,0.0f };
			obstacle3.badThing = { 0.0f,0.0f,0.0f,0.0f };
			shuriken.badThing = { 0.0f,0.0f,0.0f,0.0f };
			//-------------------------------
			speedObstacles = 250.0f;
			heigthRoof = (float)(toPoseLayer0);
			heigthPipe = (float)(HEIGTH / 2);
			heigthWaterTank = (float)(HEIGTH * 1.3);
			heigthRooftopDoor = (float)(HEIGTH * 1.5);
			widthShuriken = static_cast<float>(actualWidth / 20);
			heightShuriken = static_cast<float>(actualHeigth / 13);
			obstacle1.badThing.height = heigthRoof;
			randPosY0 = static_cast<float>(toPoseLayer0);
			randPosY1 = static_cast<float>(toPoseLayer1);
			randPosY2 = static_cast<float>(toPoseLayer2);
		}
		void initObstacle(){
			speedObstacles = 250.0f;
			heigthRoof = (float)(toPoseLayer0);
			heigthPipe = (float)(HEIGTH / 2);
			heigthWaterTank = (float)(HEIGTH * 1.3);
			heigthRooftopDoor = (float)(HEIGTH * 1.5);
			widthShuriken = static_cast<float>(actualWidth / 20);
			heightShuriken = static_cast<float>(actualHeigth / 13);
			obstacle1.badThing.height = heigthRoof;
			randPosY0 = static_cast<float>(toPoseLayer0);
			randPosY1 = static_cast<float>(toPoseLayer1);
			randPosY2 = static_cast<float>(toPoseLayer2);
			colorType = WHITE;
			colorType2 = WHITE;
			shuriken.typeObs = colectable;
			obstacle1.isPlaced = false;
			if(!alreadyLoaded)
				loadTextures();
			ROOFFALLtextureY = toPoseLayer0 - 8;
		}
		//------------------------------
		static void createRoofdrrop() {
			if (!obstacle1.isPlaced && Player::guy.score % 2) {
				randomTypeObs1 = 1;
				randomXObs1 = (float)GetRandomValue(actualWidth, actualWidth + 100);
				randomYObs1 = 1;

				switch (randomYObs1)
				{
				case 1:
					randomYObs1 = (int)randPosY1;
					obstacle1.layer = 1;
					break;
				}

				switch (randomTypeObs1)
				{
				case 1:
					obstacle1.typeObs = roofdrop;
					colorType = RED;
					obstacle1.badThing = { randomXObs1,static_cast<float>(randPosY1 - 100),(float)(speedObstacles / 3),heigthRoof };
					obstacle1.badThingTexture.width = (int)(speedObstacles / 3);
					obstacle1.isPlaced = true;
					break;
				}
			}
		}
		//------------------------------
		static void createCollectable() {
			if (!shuriken.isPlaced) {

				if (obstacle2.isPlaced) {

					switch (obstacle2.typeObs)	
					{
					case pipe:
						posXShuri = static_cast<float>(obstacle2.badThing.x + (obstacle2.badThing.width/1.5f));
						posYShuri = static_cast<int>(obstacle2.badThing.y - (obstacle2.badThing.height + 50));
						shuriken.badThing = {posXShuri,(float)posYShuri, widthShuriken, heightShuriken};
						shuriken.isPlaced = true;
						break;
					case waterTank:
						posXShuri = static_cast<float>(obstacle2.badThing.x + obstacle2.badThing.width);
						posYShuri = static_cast<int>(obstacle2.badThing.y + (obstacle2.badThing.height / 1.5f));
						shuriken.badThing = {posXShuri,(float)posYShuri,widthShuriken ,heightShuriken};
						shuriken.isPlaced = true;
						break;
					case rooftopDoor:
						posXShuri = static_cast<float>(obstacle2.badThing.x - obstacle2.badThing.width);
						posYShuri = static_cast<int>(obstacle2.badThing.y + (obstacle2.badThing.height / 2));
						shuriken.badThing = {posXShuri,(float)posYShuri,widthShuriken ,heightShuriken };
						shuriken.isPlaced = true;
						break;
					}
					shuriken.layer = obstacle2.layer;
					shuriken.badThingTexture = auxTextureObstacle2[3];
				}
				else if (obstacle3.isPlaced) {
					switch (obstacle2.typeObs)
					{
					case pipe:
						posXShuri = static_cast<float>(obstacle3.badThing.x + (obstacle3.badThing.width / 1.5f));
						posYShuri = static_cast<int>(obstacle3.badThing.y - (obstacle3.badThing.height + 50));
						shuriken.badThing = {posXShuri,(float)posYShuri,widthShuriken ,heightShuriken };
						shuriken.isPlaced = true;
						break;
					case waterTank:
						posXShuri = static_cast<float>(obstacle3.badThing.x + obstacle3.badThing.width);
						posYShuri = static_cast<int>(obstacle3.badThing.y + (obstacle3.badThing.height / 1.5f));
						shuriken.badThing = {posXShuri,(float)posYShuri,widthShuriken ,heightShuriken };
						shuriken.isPlaced = true;
						break;
					case rooftopDoor:
						posXShuri = static_cast<float>(obstacle3.badThing.x - obstacle3.badThing.width);
						posYShuri = static_cast<int>(obstacle3.badThing.y + (obstacle3.badThing.height / 2));
						shuriken.badThing = {posXShuri,(float)posYShuri,widthShuriken ,heightShuriken };
						shuriken.isPlaced = true;
						break;
					}
					shuriken.layer = obstacle3.layer;
					shuriken.badThingTexture = auxTextureObstacle2[3];
				}
				else {
					posXShuri = static_cast<float>(GetRandomValue(static_cast<int>(actualWidth + 100), static_cast<int>(actualWidth + 700)));
					posYShuri = GetRandomValue(0, 2);

					switch (posYShuri)
					{
					case 0:
						posYShuri = (int)randPosY0;
						shuriken.layer = 0;
						break;
					case 1:
						posYShuri = (int)randPosY1;
						shuriken.layer = 1;
						break;
					case 2:
						posYShuri = (int)randPosY2;
						shuriken.layer = 2;
						break;
					}
					shuriken.badThing = { posXShuri,(float)posYShuri,widthShuriken ,heightShuriken };
					shuriken.isPlaced = true;
					shuriken.layer = obstacle2.layer;
					shuriken.badThingTexture = auxTextureObstacle2[3];
				}
			}
		}
		//------------------------------
		void genObstacles() {
			//---------------
			createRoofdrrop();
			//---------------
			createObstacle(obstacle2, randomTypeObs2, randomXObs2, randomYObs2);
			//---------------
			createObstacle(obstacle3, randomTypeObs3, randomXObs3, randomYObs3);
			//---------------
			if(!Player::colectablePicked) createCollectable();
			//---------------
		}
		//------------------------------
		void createObstacle(OBSTACLES &obstacle,int randType,float randX, int randY) {
			if (!obstacle.isPlaced) {
				if (Player::guy.score <= 5000)
					randType = GetRandomValue(1, 2);
				else
					randType = GetRandomValue(1, 3);

				randX = (float)GetRandomValue(actualWidth + 100, actualWidth + 500);
				randY = GetRandomValue(0, 2);

				switch (randY)
				{
				case 0:
					randY = (int)randPosY0;
					obstacle.layer = 0;
					break;
				case 1:
					randY = (int)randPosY1;
					obstacle.layer = 1;
					break;
				case 2:
					randY = (int)randPosY2;
					obstacle.layer = 2;
					break;
				}

				switch (randType)
				{
				case 1:
					obstacle.typeObs = pipe;
					colorType2 = ORANGE;
					obstacle.badThing = { randX, (float)(randY - heigthPipe),(float)(actualWidth / 17.0f),heigthPipe };
					obstacle.badThingTexture.width = (int)(actualWidth / 17.0f);
					obstacle.isPlaced = true;
					obstacle.badThingTexture = auxTextureObstacle2[1];
					break;
				case 2:
					obstacle.typeObs = rooftopDoor;
					colorType2 = WHITE;
					obstacle.badThing = { randX, (float)(randY - heigthRooftopDoor),(float)(actualWidth / 17.0f),heigthRooftopDoor };
					obstacle.badThingTexture.width = (int)(actualWidth / 17.0f);
					obstacle.isPlaced = true;
					obstacle.badThingTexture = auxTextureObstacle2[2];
					break;
				case 3:
					obstacle.typeObs = waterTank;
					colorType2 = PINK;
					obstacle.badThing = { randX, (float)(randY - heigthWaterTank),(float)(actualWidth / 19.2f),heigthWaterTank };
					obstacle.badThingTexture.width = (int)(actualWidth / 19.2f);
					obstacle.isPlaced = true;
					obstacle.badThingTexture = auxTextureObstacle2[0];
					break;
				}
			}
		}
		//------------------------------
		void update(){
			//-------------------------
			if (!resiszeAfterResolu) {
				genObstacles();
			}
			else{
				loadTextures();
				genObstacles();
			}
			//-------------------------
			updateObstacles(obstacle1);
			//-------------------------
			updateObstacles(obstacle2);
			//-------------------------
			updateObstacles(obstacle3);
			//-------------------------
			updateObstacles(shuriken);
			//-------------------------
		}
		//------------------------------
		void updateObstacles(OBSTACLES& obstacle) {
			if (obstacle.badThing.x <= 0 - obstacle.badThing.width) {
				if (obstacle.layer == shuriken.layer) {
					obstacle.isPlaced = false;
					Player::colectablePicked = false;
				}
				else {
					obstacle.isPlaced = false;
				}
			}
			else {
				if (!Player::guy.falling && Player::guy.alive)
					obstacle.badThing.x -= ((speedObstacles)* GetFrameTime());
			}
		}
		//------------------------------
		void draw(){
			if(obstacle2.isPlaced)DrawTexture(obstacle2.badThingTexture, static_cast<int>(obstacle2.badThing.x), static_cast<int>(obstacle2.badThing.y), WHITE);
			if(obstacle3.isPlaced)DrawTexture(obstacle3.badThingTexture, static_cast<int>(obstacle3.badThing.x), static_cast<int>(obstacle3.badThing.y), WHITE);
			if(shuriken.isPlaced) DrawTexture(shuriken.badThingTexture, static_cast<int>(shuriken.badThing.x), static_cast<int>(shuriken.badThing.y), WHITE);
#if DEBUG
			DrawRectangleLinesEx(obstacle1.badThing, 3, colorType);
			DrawRectangleLinesEx(obstacle2.badThing, 3, colorType2);
			DrawRectangleLinesEx(obstacle3.badThing, 3, colorType2);
			DrawRectangleLinesEx(shuriken.badThing, 3, colorType);
#endif
		}
		//------------------------------
		void drawRoofall(){
			DrawTexture(obstacle1.badThingTexture, static_cast<int>(obstacle1.badThing.x), static_cast<int>(ROOFFALLtextureY), WHITE);
		}
		//------------------------------
		void reziseOnResolution(){
			//-------------------------------
			obstacle1.isPlaced = false;
			obstacle2.isPlaced = false;
			obstacle3.isPlaced = false;
			shuriken.isPlaced = false;
			//-------------------------------
			obstacle1.badThing = {0.0f,0.0f,0.0f,0.0f};
			obstacle2.badThing = {0.0f,0.0f,0.0f,0.0f};
			obstacle3.badThing = {0.0f,0.0f,0.0f,0.0f};
			shuriken.badThing = {0.0f,0.0f,0.0f,0.0f};
			//-------------------------------
			loadTextures();
			//-------------------------------
			randPosY0 = static_cast<float>(toPoseLayer0);
			randPosY1 = static_cast<float>(toPoseLayer1);
			randPosY2 = static_cast<float>(toPoseLayer2);
			heigthRoof = (float)(toPoseLayer0); 
			heigthPipe = (float)(HEIGTH / 2);
			heigthWaterTank = (float)(HEIGTH * 1.3);
			heigthRooftopDoor = (float)(HEIGTH * 1.5);
			widthShuriken = static_cast<float>(actualWidth / 20);
			heightShuriken = static_cast<float>(actualHeigth / 13);
			ROOFFALLtextureY = toPoseLayer0 - 8;
		}
		//------------------------------
		void loadTextures(){
			//717 PIXELES DONDE EMPIEZA EL TECHO DEL BACKGROUND 3  en 1080 de alto
			//363 PIXELES BASES ROOFFALL A 1080 DE ALTO
			Image image;
			if (!onRezise && !resiszeAfterResolu) {
				//---------
				image = LoadImage("res/assets/obstacles/roofFall.png");
				ImageResize(&image, static_cast<int>(speedObstacles / 3), static_cast<int>(heigthRoof));
				obstacle1.badThingTexture = LoadTextureFromImage(image);
				UnloadImage(image);
				//---------
				image = LoadImage("res/assets/obstacles/waterTank.png");
				ImageResize(&image, static_cast<int>(actualWidth / 19.2f), static_cast<int>(heigthWaterTank));
				auxTextureObstacle2[0] = LoadTextureFromImage(image);
				UnloadImage(image);
				//---------
				image = LoadImage("res/assets/obstacles/pipeObstacle.png");
				ImageResize(&image, static_cast<int>(actualWidth / 17.0f), static_cast<int>(heigthPipe));
				auxTextureObstacle2[1] = LoadTextureFromImage(image);
				UnloadImage(image);
				//---------
				image = LoadImage("res/assets/obstacles/rooftopDoor.png");
				ImageResize(&image, static_cast<int>(actualWidth / 17.0f), static_cast<int>(heigthRooftopDoor));
				auxTextureObstacle2[2] = LoadTextureFromImage(image);
				UnloadImage(image);
				//---------
				image = LoadImage("res/assets/obstacles/shuriken.png");
				ImageResize(&image, static_cast<int>(widthShuriken), static_cast<int>(heightShuriken));
				auxTextureObstacle2[3] = LoadTextureFromImage(image);
				UnloadImage(image);
				//---------
			}
			else if(resiszeAfterResolu || onRezise){
				unloadTextures();
				//---------
				image = LoadImage("res/assets/obstacles/roofFall.png");
				ImageResize(&image, static_cast<int>(speedObstacles / 3), static_cast<int>(heigthRoof));
				obstacle1.badThingTexture = LoadTextureFromImage(image);
				UnloadImage(image);
				//---------
				image = LoadImage("res/assets/obstacles/waterTank.png");
				ImageResize(&image, static_cast<int>(actualWidth / 19.2f), static_cast<int>(heigthWaterTank));
				auxTextureObstacle2[0] = LoadTextureFromImage(image);
				UnloadImage(image);
				//---------
				image = LoadImage("res/assets/obstacles/pipeObstacle.png");
				ImageResize(&image, static_cast<int>(actualWidth / 12.5f), static_cast<int>(heigthPipe));
				auxTextureObstacle2[1] = LoadTextureFromImage(image);
				UnloadImage(image);
				//---------
				image = LoadImage("res/assets/obstacles/rooftopDoor.png");
				ImageResize(&image, static_cast<int>(actualWidth / 11.4f), static_cast<int>(heigthRooftopDoor));
				auxTextureObstacle2[2] = LoadTextureFromImage(image);
				UnloadImage(image);
				//---------
				image = LoadImage("res/assets/obstacles/shuriken.png");
				ImageResize(&image, static_cast<int>(widthShuriken), static_cast<int>(heightShuriken));
				auxTextureObstacle2[3] = LoadTextureFromImage(image);
				UnloadImage(image);
				//---------
			}
			alreadyLoaded = true;
			resiszeAfterResolu = false;
		}
		//------------------------------
		void unloadTextures(){
			//-------------------------------
			UnloadTexture(auxTextureObstacle2[0]);
			UnloadTexture(auxTextureObstacle2[1]);
			UnloadTexture(auxTextureObstacle2[2]);
			UnloadTexture(auxTextureObstacle2[3]);
			//-------------------------------
			UnloadTexture(obstacle1.badThingTexture);
			UnloadTexture(obstacle2.badThingTexture);
			UnloadTexture(obstacle3.badThingTexture);
			UnloadTexture(shuriken.badThingTexture);
			//-------------------------------
		}
		//------------------------------

	}
}