#ifndef BUTTONS_H
#define BUTTONS_H

#include "raylib.h"

#define PAUSEBLACK CLITERAL(Color){ 0 , 0 , 0 , 155 }

namespace Endless {
	namespace ButtonHandle {
		//-----------------------
		extern Rectangle pauseBlind;
		//-----------------------
		const int state = 2;
		//-----------------------
		struct BUTTONS{
			Texture2D BTN[state];
			Texture2D BTN2;
			Vector2 POS;
			bool state;
		};
		//-----------------------
		//MENU
		extern BUTTONS START;
		extern BUTTONS OPTIONS;
		extern BUTTONS CREDITS;
		extern BUTTONS EXIT;
		//-----------------------
		//GAMEPLAY
		extern BUTTONS GOMENU;
		extern BUTTONS RESUME;
		extern BUTTONS RESTART;
		//-----------------------
		//OPTIONS
		extern BUTTONS HELP;
		extern BUTTONS MUSIC;
		extern BUTTONS SCREEN;
		extern BUTTONS BACK;
		//-----------------------
		//RESOLUTIONS
		extern BUTTONS _800X600;
		extern BUTTONS _1080X720;
		extern BUTTONS _1440X900;
		//-----------------------
		extern BUTTONS backRigth;
		extern BUTTONS escExit;
		extern BUTTONS menuVolume;
		extern BUTTONS gameVolume;
		//-----------------------

		void init();
		void load();
		void draw_mmenu();
		void draw_optionsmenu();
		void draw_pause();
		void draw_resolutions();
		void draw_volumeOp();
		void draw_exitThings(int whatExit);
		void draw_restart();
		void resizeOnResolution();
		void unload();
	}
}
#endif // !BUTTONS_H