#include "Menu/Background/scenes.h"

#include "Gameplay/Screen/screen.h"

using namespace Endless;
using namespace ScreenHandle;

namespace Endless {
	namespace SceneManagerMenu {
		//----------------------
		MENU_THINGS menu;
		//----------------------

		void init(){
			menu.posBackground = { 0.0f,0.0f };
		}
		//----------------------
		void load(){
			Image image;
			if (!ScreenHandle::onRezise) {
				//------------------------
				image = LoadImage("res/assets/background/bakcgroundMenu.png");
				ImageResize(&image, actualWidth, actualHeigth);
				menu.background = LoadTextureFromImage(image);
				UnloadImage(image);
				//------------------------
				image = LoadImage("res/assets/background/creditsMenu.png");
				ImageResize(&image, actualWidth, actualHeigth);
				menu.credits = LoadTextureFromImage(image);
				UnloadImage(image);
				//------------------------
				image = LoadImage("res/assets/background/helpMenu.png");
				ImageResize(&image, actualWidth, actualHeigth);
				menu.help = LoadTextureFromImage(image);
				UnloadImage(image);
				//------------------------
			}
			else{
				//------------------------
				unload();
				//------------------------
				image = LoadImage("res/assets/background/bakcgroundMenu.png");
				ImageResize(&image, actualWidth, actualHeigth);
				menu.background = LoadTextureFromImage(image);
				UnloadImage(image);
				//------------------------
				image = LoadImage("res/assets/background/creditsMenu.png");
				ImageResize(&image, actualWidth, actualHeigth);
				menu.credits = LoadTextureFromImage(image);
				UnloadImage(image);
				//------------------------
				image = LoadImage("res/assets/background/helpMenu.png");
				ImageResize(&image, actualWidth, actualHeigth);
				menu.help = LoadTextureFromImage(image);
				UnloadImage(image);
				//------------------------
			}
		}
		//----------------------
		void unload(){
			UnloadTexture(menu.background);
			UnloadTexture(menu.credits);
			UnloadTexture(menu.help);
		}
		//----------------------
		void draw(){
			DrawTexture(menu.background, static_cast<int>(menu.posBackground.x), static_cast<int>(menu.posBackground.y), WHITE);
		}
		//----------------------
		void drawCredits(){
			DrawTextureEx(menu.credits, menu.posBackground, 0.0f, 1.0f, WHITE);
		}
		//----------------------
		void drawHelp(){
			DrawTextureEx(menu.help, menu.posBackground, 0.0f, 1.0f, WHITE);
		}
		//----------------------
		void reziseOnResolu(){
			load();
		}
		//----------------------
	}
}