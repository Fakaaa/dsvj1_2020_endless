#ifndef GAMELOOP_H
#define GAMELOOP_H

namespace Endless {
	namespace GameLoop {

		extern bool inGame;
		extern void game();
	}
}
#endif // !GAMELOOP_H