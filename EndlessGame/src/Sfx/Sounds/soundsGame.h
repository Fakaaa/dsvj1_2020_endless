#ifndef SOUNDSGAME_H
#define SOUNDSGAME_H

#include "raylib.h" 

namespace Endless {
	namespace SoundsManager {
		//--------------------
		struct SOUNDS_MENU{
			Sound select;
			Sound enter;
			Sound back;
		};
		//--------------------
		struct SOUNDS_PLAYER{
			Sound footsteps;
			Sound jump;
			Sound getCollect;
		};
		//--------------------
		extern SOUNDS_PLAYER playerSfx;
		//--------------------
		extern SOUNDS_MENU menuSfx;
		//--------------------
		void initSounds();
		void loadSounds();
		//--------GAME---------
			void playFootsteps();
			void playGetShuri();
			void playJump();
		//----------
			void stopFootsteps();
			void stopGetShuri();
			void stopJump();
		//--------MENU----------
			void playSelect();
			void playBack();
			void playEnter();
			//----------
			void stopSelect();
			void stopBack();
			void stopEnter();
		//--------------------
		void setVolumeSounds();
		void unloadSounds();
	}
}
#endif // !SOUNDSGAME_H