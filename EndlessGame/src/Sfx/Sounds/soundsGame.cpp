#include "soundsGame.h"

namespace Endless {
	namespace SoundsManager {
		//------------------------------
		SOUNDS_PLAYER playerSfx;
		//------------------------------
		SOUNDS_MENU menuSfx;
		//------------------------------
		//------------------------------
		void initSounds(){
			setVolumeSounds();
		}
		//------------------------------
		void loadSounds() {
			playerSfx.footsteps = LoadSound("res/assets/audio/sounds/runPlayer.ogg");
			playerSfx.jump =	  LoadSound("res/assets/audio/sounds/jumpPlayer.ogg");
			playerSfx.getCollect = LoadSound("res/assets/audio/sounds/shuriken.ogg");
			menuSfx.back =		  LoadSound("res/assets/audio/sounds/back.ogg");
			menuSfx.enter =		  LoadSound("res/assets/audio/sounds/enter.ogg");
			menuSfx.select =	  LoadSound("res/assets/audio/sounds/select.ogg");
		}
		//------------------------------
		void playFootsteps(){
			if (!IsSoundPlaying(playerSfx.footsteps))
				PlaySound(playerSfx.footsteps);
		}
		//------------------------------
		void playGetShuri(){
			if (!IsSoundPlaying(playerSfx.getCollect))
				PlaySound(playerSfx.getCollect);
		}
		//------------------------------
		void playJump(){
			if (!IsSoundPlaying(playerSfx.jump))
				PlaySound(playerSfx.jump);
		}
		//------------------------------
		void stopFootsteps(){
			if (IsSoundPlaying(playerSfx.footsteps))
				StopSound(playerSfx.footsteps);
		}
		//------------------------------
		void stopGetShuri(){
			if (IsSoundPlaying(playerSfx.getCollect))
				StopSound(playerSfx.getCollect);
		}
		//------------------------------
		void stopJump(){
			if (IsSoundPlaying(playerSfx.jump))
				StopSound(playerSfx.jump);
		}
		//------------------------------
		void playSelect(){
			if (!IsSoundPlaying(menuSfx.select) && !IsSoundPlaying(menuSfx.enter) && !IsSoundPlaying(menuSfx.back))
				PlaySound(menuSfx.select);
		}
		//------------------------------
		void playBack(){
			if (!IsSoundPlaying(menuSfx.select) && !IsSoundPlaying(menuSfx.enter) && !IsSoundPlaying(menuSfx.back))
				PlaySound(menuSfx.back);
		}
		//------------------------------
		void playEnter(){
			if (!IsSoundPlaying(menuSfx.select) && !IsSoundPlaying(menuSfx.enter) && !IsSoundPlaying(menuSfx.back))
				PlaySound(menuSfx.enter);
		}
		//------------------------------
		void stopSelect(){
			if (IsSoundPlaying(menuSfx.select))
				StopSound(menuSfx.select);
		}
		//------------------------------
		void stopBack(){
			if (IsSoundPlaying(menuSfx.back))
				StopSound(menuSfx.back);
		}
		//------------------------------
		void stopEnter(){
			if (IsSoundPlaying(menuSfx.enter))
				StopSound(menuSfx.enter);
		}
		//------------------------------
		void setVolumeSounds(){
			SetSoundVolume(playerSfx.footsteps, 0.2f);
			SetSoundVolume(playerSfx.jump, 0.2f);
			SetSoundVolume(playerSfx.getCollect, 0.2f);
			SetSoundVolume(menuSfx.select, 0.3f);
			SetSoundVolume(menuSfx.back, 0.3f);
			SetSoundVolume(menuSfx.enter, 0.3f);
		}
		//------------------------------
		void unloadSounds(){
			UnloadSound(playerSfx.footsteps);
			UnloadSound(playerSfx.jump);
			UnloadSound(playerSfx.getCollect);
			UnloadSound(menuSfx.back);
			UnloadSound(menuSfx.enter);
			UnloadSound(menuSfx.select);
		}
	}
}