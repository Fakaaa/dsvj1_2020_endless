#ifndef MUSICGAME_H
#define MUSICGAME_H

#include "raylib.h"

namespace Endless {
	namespace MusicHandle {
		//-------------------
		const int states = 6;
		//-------------------
		enum VOLUME{
			off,
			_1,
			_2,
			_3,
			_4,
			_5
		};
		//-------------------
		enum WHAT_VOLUME {
			MENU,
			GAME
		};
		//-------------------
		extern VOLUME stateMenuVolume;
		//-------------------
		extern WHAT_VOLUME whatVlm;
		//-------------------
		struct MUSIC_MENU{
			Music soundTrack;
			Texture2D volumeState[states];
			Vector2 posTextureVolume;
			float volumeMenu;
			float volumeGame;
		};
		//-------------------
		extern MUSIC_MENU soudntrackMenu;
		extern MUSIC_MENU soudntrackGame;
		//-------------------
		void init();
		void load();
		void loadTexture();
		void drawMusicMenu();
		void drawMusicGame();
		void update();
		void setMenuMusicVolume();
		void unload();
		void unloadTexture();
		void resizeOnChangeRes();
		void deinit();
	}
}
#endif // !MUSICGAME_H