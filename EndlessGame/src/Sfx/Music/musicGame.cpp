#include <iostream>	

#include "Sfx/Music/musicGame.h"

#include "Menu/menuHandeling.h"
#include "Gameplay/gameplayHandle.h"
#include "Gameplay/Screen/screen.h"
#include "Menu/Buttons/buttons.h"

using namespace Endless;
using namespace ScreenHandle;
using namespace ButtonHandle;

namespace Endless {
	namespace MusicHandle {
		//-------------------
		MUSIC_MENU soudntrackMenu;
		//-------------------
		MUSIC_MENU soudntrackGame;
		//-------------------
		VOLUME stateMenuVolume;
		VOLUME stateGameVolume;
		//-------------------
		WHAT_VOLUME whatVlm;
		//-------------------
		int volumeLvl = -1;
		//-------------------
		void init() {
			load();
		}
		//---------------------------------
		void load() {
			soudntrackMenu.soundTrack = LoadMusicStream("res/assets/audio/music/menu/menuSong.mp3");
			soudntrackGame.soundTrack = LoadMusicStream("res/assets/audio/music/gameplay/gameplay0.mp3");
			stateMenuVolume = _5;
			soudntrackMenu.volumeMenu = 0.5;
			soudntrackGame.volumeGame = 0.5;
			SetMusicVolume(soudntrackMenu.soundTrack,soudntrackMenu.volumeMenu);
			SetMusicVolume(soudntrackGame.soundTrack, soudntrackGame.volumeGame);
			if (MenuHanlde::onMenu) {
				PlayMusicStream(soudntrackMenu.soundTrack);
			}
			soudntrackMenu.posTextureVolume = { static_cast<float>(actualWidth / 2.0f) - (MUSIC.BTN->width / 2), static_cast<float>(actualHeigth / 3) };
			soudntrackGame.posTextureVolume = { static_cast<float>(actualWidth / 2.0f) - (MUSIC.BTN->width / 2), static_cast<float>(actualHeigth / 2) };
			loadTexture();
		}
		//---------------------------------
		void loadTexture(){
			//--------------------------------
			Image image[states];
			//--------------------------------
			if (!ScreenHandle::onRezise) {
				//-----
				for (int i = 0; i < states; i++) {
					image[i] = LoadImage(FormatText("res/assets/audio/music/menu/musicVolume%i.png", i));
					ImageResize(&image[i], static_cast<int>(actualWidth / 6), static_cast<int>(actualWidth / 8));
					soudntrackMenu.volumeState[i] = LoadTextureFromImage(image[i]);
					UnloadImage(image[i]);
				}
				//-----
				for (int i = 0; i < states; i++) {
					image[i] = LoadImage(FormatText("res/assets/audio/music/menu/musicGame%i.png", i));
					ImageResize(&image[i], static_cast<int>(actualWidth / 6), static_cast<int>(actualWidth / 8));
					soudntrackGame.volumeState[i] = LoadTextureFromImage(image[i]);
					UnloadImage(image[i]);
				}
				//-----
			}
			else {
				//-----
				unloadTexture();
				//-----
				for (int i = 0; i < states; i++) {
					image[i] = LoadImage(FormatText("res/assets/audio/music/menu/musicVolume%i.png", i));
					ImageResize(&image[i], static_cast<int>(actualWidth / 6), static_cast<int>(actualWidth / 8));
					soudntrackMenu.volumeState[i] = LoadTextureFromImage(image[i]);
					UnloadImage(image[i]);
				}
				//-----
				for (int i = 0; i < states; i++) {
					image[i] = LoadImage(FormatText("res/assets/audio/music/menu/musicGame%i.png", i));
					ImageResize(&image[i], static_cast<int>(actualWidth / 6), static_cast<int>(actualWidth / 8));
					soudntrackGame.volumeState[i] = LoadTextureFromImage(image[i]);
					UnloadImage(image[i]);
				}
				//-----
			}
			//--------------------------------
		}
		//---------------------------------
		void drawMusicMenu(){
			switch (stateMenuVolume)
			{
			case Endless::MusicHandle::off:
				DrawTextureEx(soudntrackMenu.volumeState[0], soudntrackMenu.posTextureVolume, 0.0f, 1.0f, WHITE);
				break;
			case Endless::MusicHandle::_1:
				DrawTextureEx(soudntrackMenu.volumeState[1], soudntrackMenu.posTextureVolume, 0.0f, 1.0f, WHITE);
				break;
			case Endless::MusicHandle::_2:
				DrawTextureEx(soudntrackMenu.volumeState[2], soudntrackMenu.posTextureVolume, 0.0f, 1.0f, WHITE);
				break;
			case Endless::MusicHandle::_3:
				DrawTextureEx(soudntrackMenu.volumeState[3], soudntrackMenu.posTextureVolume, 0.0f, 1.0f, WHITE);
				break;
			case Endless::MusicHandle::_4:
				DrawTextureEx(soudntrackMenu.volumeState[4], soudntrackMenu.posTextureVolume, 0.0f, 1.0f, WHITE);
				break;
			case Endless::MusicHandle::_5:
				DrawTextureEx(soudntrackMenu.volumeState[5], soudntrackMenu.posTextureVolume, 0.0f, 1.0f, WHITE);
				break;
			default:
				std::cout << "ERROR10: Fallo en el switch del dibujado del voluemn!" << std::endl;
				break;
			}
		}
		//---------------------------------
		void drawMusicGame(){
			switch (stateGameVolume)
			{
			case Endless::MusicHandle::off:
				DrawTextureEx(soudntrackGame.volumeState[0], soudntrackGame.posTextureVolume, 0.0f, 1.0f, WHITE);
				break;
			case Endless::MusicHandle::_1:
				DrawTextureEx(soudntrackGame.volumeState[1], soudntrackGame.posTextureVolume, 0.0f, 1.0f, WHITE);
				break;
			case Endless::MusicHandle::_2:
				DrawTextureEx(soudntrackGame.volumeState[2], soudntrackGame.posTextureVolume, 0.0f, 1.0f, WHITE);
				break;
			case Endless::MusicHandle::_3:
				DrawTextureEx(soudntrackGame.volumeState[3], soudntrackGame.posTextureVolume, 0.0f, 1.0f, WHITE);
				break;
			case Endless::MusicHandle::_4:
				DrawTextureEx(soudntrackGame.volumeState[4], soudntrackGame.posTextureVolume, 0.0f, 1.0f, WHITE);
				break;
			case Endless::MusicHandle::_5:
				DrawTextureEx(soudntrackGame.volumeState[5], soudntrackGame.posTextureVolume, 0.0f, 1.0f, WHITE);
				break;
			default:
				std::cout << "ERROR10: Fallo en el switch del dibujado del volumen!" << std::endl;
				break;
			}
		}
		//---------------------------------
		void update() {
			if (MenuHanlde::onMenu) {
				UpdateMusicStream(soudntrackMenu.soundTrack);
			}
			else {
				StopMusicStream(soudntrackMenu.soundTrack);
			}
			if (GameManager::onGameplay) {
				UpdateMusicStream(soudntrackGame.soundTrack);
			}
			else {
				StopMusicStream(soudntrackGame.soundTrack);
			}
		}
		//---------------------------------
		void setMenuMusicVolume() {
			//---------------------
			if (!MenuHanlde::onMusicGame && !MenuHanlde::onMusicMenu) MenuHanlde::handleKeys(MenuHanlde::whatVolume, MusicHandle::MENU, MusicHandle::GAME);
			//---------------------
			switch (MenuHanlde::whatVolume)
			{
			case MENU:
				//---------------------
				ButtonHandle::menuVolume.state = true;
				ButtonHandle::gameVolume.state = false;

				if (IsKeyPressed(KEY_ENTER) && MenuHanlde::enterMenuVolume == 0) {
					MenuHanlde::onMusicMenu = true;
					MenuHanlde::onMusicGame = false;
					//MenuHanlde::onShowMusic = false;
					MenuHanlde::selectThat = false;
				}
				if (MenuHanlde::onMusicMenu) {
					if (MenuHanlde::onMusicMenu) MenuHanlde::handleKeyMusic(MenuHanlde::onmusicKeyMenu, MusicHandle::off, MusicHandle::_5);

					switch (MenuHanlde::onmusicKeyMenu)
					{
					case 0:
						stateMenuVolume = off;
						soudntrackMenu.volumeMenu = 0.0f;
						SetMusicVolume(soudntrackMenu.soundTrack, soudntrackMenu.volumeMenu);
						break;
					case 1:
						stateMenuVolume = _1;
						soudntrackMenu.volumeMenu = 0.1f;
						SetMusicVolume(soudntrackMenu.soundTrack, soudntrackMenu.volumeMenu);
						break;
					case 2:
						stateMenuVolume = _2;
						soudntrackMenu.volumeMenu = 0.2f;
						SetMusicVolume(soudntrackMenu.soundTrack, soudntrackMenu.volumeMenu);
						break;
					case 3:
						stateMenuVolume = _3;
						soudntrackMenu.volumeMenu = 0.3f;
						SetMusicVolume(soudntrackMenu.soundTrack, soudntrackMenu.volumeMenu);
						break;
					case 4:
						stateMenuVolume = _4;
						soudntrackMenu.volumeMenu = 0.4f;
						SetMusicVolume(soudntrackMenu.soundTrack, soudntrackMenu.volumeMenu);
						break;
					case 5:
						stateMenuVolume = _5;
						soudntrackMenu.volumeMenu = 0.5f;
						SetMusicVolume(soudntrackMenu.soundTrack, soudntrackMenu.volumeMenu);
						break;
					}
					if (IsKeyPressed(KEY_RIGHT)) {
						MenuHanlde::onMusicMenu = false;
						MenuHanlde::enterMenuVolume = 1;
						MenuHanlde::selectThat = false;
					}
				}
				if (MenuHanlde::whatVolume == MENU)
					MenuHanlde::enterMenuVolume = 0;
				else
					MenuHanlde::enterMenuVolume = 1;
				//---------------------
				break;
			case GAME:
				//---------------------
				ButtonHandle::menuVolume.state = false;
				ButtonHandle::gameVolume.state = true;

				if (IsKeyPressed(KEY_ENTER) && MenuHanlde::enterGameVolume == 0) {
					MenuHanlde::onMusicMenu = false;
					MenuHanlde::onMusicGame = true;
					MenuHanlde::selectThat = false;
				}
				//---------------------
				if (MenuHanlde::onMusicGame) {
					if (MenuHanlde::onMusicGame) MenuHanlde::handleKeyMusic(MenuHanlde::onmusicKeyGame, MusicHandle::off, MusicHandle::_5);

					switch (MenuHanlde::onmusicKeyGame)
					{
					case 0:
						stateGameVolume = off;
						soudntrackGame.volumeMenu = 0.0f;
						SetMusicVolume(soudntrackGame.soundTrack, soudntrackGame.volumeMenu);
						break;
					case 1:
						stateGameVolume = _1;
						soudntrackGame.volumeMenu = 0.1f;
						SetMusicVolume(soudntrackGame.soundTrack, soudntrackGame.volumeMenu);
						break;
					case 2:
						stateGameVolume = _2;
						soudntrackGame.volumeMenu = 0.2f;
						SetMusicVolume(soudntrackGame.soundTrack, soudntrackGame.volumeMenu);
						break;
					case 3:
						stateGameVolume = _3;
						soudntrackGame.volumeMenu = 0.3f;
						SetMusicVolume(soudntrackGame.soundTrack, soudntrackGame.volumeMenu);
						break;
					case 4:
						stateGameVolume = _4;
						soudntrackGame.volumeMenu = 0.4f;
						SetMusicVolume(soudntrackGame.soundTrack, soudntrackGame.volumeMenu);
						break;
					case 5:
						stateGameVolume = _5;
						soudntrackGame.volumeMenu = 0.5f;
						SetMusicVolume(soudntrackGame.soundTrack, soudntrackGame.volumeMenu);
						break;
					}
					if (IsKeyPressed(KEY_RIGHT)) {
						MenuHanlde::onMusicGame = false;
						MenuHanlde::enterGameVolume = 1;
						MenuHanlde::selectThat = false;
					}
				}
				if (MenuHanlde::whatVolume == GAME)
					MenuHanlde::enterGameVolume = 0;
				else
					MenuHanlde::enterGameVolume = 1;
				//---------------------
				break;
			}
			
			if (MenuHanlde::onShowMusic)
				MenuHanlde::enterMusic = 0;
			else
				MenuHanlde::enterMusic = 1;
		}
		//---------------------------------
		void unload() {
			UnloadMusicStream(soudntrackMenu.soundTrack);
			UnloadMusicStream(soudntrackGame.soundTrack);
		}
		//---------------------------------
		void unloadTexture(){
			for (int i = 0; i < states; i++){
				UnloadTexture(soudntrackMenu.volumeState[states]);
			}
		}
		//---------------------------------
		void resizeOnChangeRes(){
			loadTexture();
			soudntrackMenu.posTextureVolume = { static_cast<float>(actualWidth / 2.0f) - (MUSIC.BTN->width / 2), static_cast<float>(actualHeigth / 3) };
		}
		//---------------------------------
		void deinit(){
			unload();
			unloadTexture();
		}
		//---------------------------------
	}
}